```shell script
sudo docker run --name kubia-container -p 8080:8080 -d kubia
sudo docker exec -it kubia-container bash

minikube start --driver=virtualbox --cpus=8 

kubectl cluster-info

kubectl run kubia --image=toliyansky/kubia --port=8080 --generator=run/v1

kubectl get pods

kubectl describe pod podname

kubectl expose rc kubia --type=LoadBalancer --name kubia-http

kubectl get services

minikube service kubia-http

kubectl create -f kubia-manual.yml 

# from to OR host:port pod:port
kubectl port-forward kubia-manual 8888:8080

kubectl get pods --show-labels

kubectl get pods -L creation_method,env

kubectl label pod kubia-manual creation_method=manual

kubectl label pod kubia-manual-v2 env=debug --overwrite

kubectl get nodes

kubectl label node lalal gpu=ture


kubectl annotate pod kubia-gpu mycompany.com/someannotations="foo bar"

kubectl describe pod kubia-manual | grep Annot

kubectl get ns

kubectl get pods --namespace kube-system # or -n

kubectl create namespace custom-namespace

#create pod in namespace
 kubectl create -f https://gitlab.com/toliyansky/kubia/-/raw/master/kubia-manual.yml -n custom-namespace

kubectl delete pod -l creation_method=manual

kubectl delete ns custon-namespace

# 1 type 2 all 
minikube delete all --all
```
120 page